import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PortalComponent } from './portal/portal.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegisterComponent } from './_forms/register/register.component';
import { HeadersComponent } from './_elements/headers/headers.component';
import { LoggedInService } from 'src/app/_services/logged-in-service.service';
import { LoginComponent } from './_forms/login/login.component';
import { HomepageComponent } from './homepage/homepage.component';

@NgModule({
  declarations: [
    AppComponent,
    PortalComponent,
    LoginComponent,
    RegisterComponent,
    HeadersComponent,
    HomepageComponent,
  ],
  imports: [  
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule
  ],
  providers: [LoggedInService],
  bootstrap: [AppComponent]
})
export class AppModule { }
